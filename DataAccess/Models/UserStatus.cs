﻿namespace DataAccess.Models
{
    public enum UserStatus
    {
        NotVerified=0,
        Verified=1,
        Blocked=2
    }
}