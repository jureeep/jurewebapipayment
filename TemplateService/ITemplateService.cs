﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TemplateService
{
   public interface ITemplateService
    {
        Task<string> RenderTemplateAsync(string template, object obj);
    }
}
