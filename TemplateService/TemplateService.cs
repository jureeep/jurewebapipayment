﻿using Stubble.Core.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TemplateService
{
    public class TemplateService : ITemplateService
    {
        public async Task<string> RenderTemplateAsync(string template, object obj)
        {
            var stubble = new StubbleBuilder().Build();
            var output = await stubble.RenderAsync(template, obj);
            return output;
        }
    }
}
