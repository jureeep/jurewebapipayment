﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace CrossCutting
{
   public static class Serializer
    {
        public static T ParseXML<T>(string xml) where T : class
        {
            var Value = XDocument.Parse(xml);
            XNamespace ns = @"http://schemas.xmlsoap.org/soap/envelope/";
            var unwrappedResponse = Value.Descendants((XNamespace)"http://schemas.xmlsoap.org/soap/envelope/" + "Body").First().FirstNode;
            var cutDocument = XDocument.Parse(unwrappedResponse.ToString());
            XmlSerializer oXmlSerializer = new XmlSerializer(typeof(T));
            Stream s = new MemoryStream();
            cutDocument.Save(s);
            s.Seek(0, SeekOrigin.Begin);
            using (XmlReader Reader = XmlReader.Create(s))
            {
                return  (T)oXmlSerializer.Deserialize(Reader);
            }
        }
        public static string SerializeXML<T>(T obj) where T : class
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(T));
            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, obj);
                    return sww.ToString();
                }
            }
        }
    }
}
