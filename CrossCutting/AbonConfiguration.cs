﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrossCutting
{
  public class AbonConfiguration
    {
        public string ProviderId { get; set; }
        public string Path { get; set; }
        public string Pass { get; set; }
        public string PublicXMLKey { get; set; }
        public string ConfirmSoapAction { get; set; }
        public string ValidateSoapAction { get; set; }
        public string CreateSoapAction { get; set; }
        public string AbonUri { get; set; }
        public string PartnerId { get; set; }
        public string AbonCreationUri { get; set; }
        public string CouponCancelAction { get; set; }
        public string CouponCancelSoap { get; set; }
        public string CouponValidateSoap { get; set; }
        public string CreateCouponSoap { get; set; }
        public string ConfirmTransactionSoap { get; set; }
    }
}
