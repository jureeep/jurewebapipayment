﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrossCutting
{
    public class AircashConfiguration
    {
        public string PartnerID { get; set; }
        public string AircashUriRoot { get; set; }
        public string CheckUserAction { get; set; }
        public string CreatePayoutAction { get; set; }
        public string CheckStatusPayoutAction { get; set; }
        public string CertificatePath { get; set; }
    }
}
