﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.AircashPayment.Models
{
   public class CreateAndConfirmTransactionResponse
    {
        public bool Success { get; set; }
        public string PartnerTransactionID { get; set; }
        public Error Error { get; set; }
    }
}
