﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services.AircashPayment.Models
{
    public class ParameterPayment
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
