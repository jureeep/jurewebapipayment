﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.AircashPayment.Models
{
    public class CheckPlayerResponse
    {
        public bool IsPlayer { get; set; }
        public Error Error { get; set; }

    }
}
