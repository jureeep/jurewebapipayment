﻿using Services.AircashPayment.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.AircashPayment
{
   public interface IAircashPaymentService
    {
        CheckPlayerResponse CheckPlayer(List<ParameterPayment> parameters,string signature);
        CreateAndConfirmTransactionResponse CreateAndConfirmTransaction(decimal amount, List<ParameterPayment> parameters, string transactionID, string signature);
    }
}
