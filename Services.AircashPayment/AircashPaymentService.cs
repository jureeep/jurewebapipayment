﻿using CrossCutting;
using DataAccess;
using DataAccess.Models;
using Microsoft.Extensions.Options;
using Services.AircashPayment.Models;
using SignatureService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.AircashPayment
{
    public class AircashPaymentService : IAircashPaymentService
    {
        private const string GAGI = "gagi95";
        private const string USERNAME = "username";
        private const string EMAIL = "email";
        private readonly VirtualDbContext _context;
        private ISignatureService SignatureService;
        private AircashConfiguration AircashConfig;
        public AircashPaymentService(IOptionsMonitor<AircashConfiguration> aircashConfiguration, ISignatureService signatureService,VirtualDbContext context)
        {
            SignatureService = signatureService;
            AircashConfig = aircashConfiguration.CurrentValue;
            _context = context;
        }

        public CheckPlayerResponse CheckPlayer(List<ParameterPayment> parameters, string signature)
        {
     
            var toVerify = $"Parameters={String.Join(",", parameters.Select(x =>$"Key={ x.Key }&Value={ x.Value}"))}";
            //potpis provjera
            if (!SignatureService.VerifySignature(AircashConfig.CertificatePath,signature,toVerify))
            {
                return new CheckPlayerResponse
                {
                    IsPlayer = false,
                    Error = new Error {ErrorCode=1,ErrorMessage="Invalid Signature"}
                };
            }
            var checkUsernameParameter = parameters.FirstOrDefault(x => x.Key == USERNAME);
            var checkEmailParameter= parameters.FirstOrDefault(x => x.Key == EMAIL);
            User user;
            if (checkUsernameParameter!=null)
            {
                user = _context.Users.FirstOrDefault(x=>x.Username==checkUsernameParameter.Value);            
            }
            else
            {
                user = _context.Users.FirstOrDefault(x => x.Username == checkEmailParameter.Value);
            }
            if (user==null)
            {
                return new CheckPlayerResponse
                {
                    IsPlayer = false,
                    Error = new Error { ErrorCode = 1, ErrorMessage = "User not found" }
                };
            }
            return new CheckPlayerResponse
            {
                IsPlayer = true,
                Error = null
            };

        }

        public CreateAndConfirmTransactionResponse CreateAndConfirmTransaction(decimal amount, List<ParameterPayment> parameters, string transactionID, string signature)
        {
            var paramsVerifyString= $"Parameters={String.Join(",", parameters.Select(x => $"Key={ x.Key }&Value={ x.Value}"))}";
            var toVerify = $"Amount={amount}&{paramsVerifyString}&TransactionID={transactionID}";
            var partnerTransactionID = Guid.NewGuid();
            if (!SignatureService.VerifySignature(AircashConfig.CertificatePath, signature, toVerify))
            {
                return new CreateAndConfirmTransactionResponse
                {
                    Success = false,
                    PartnerTransactionID = partnerTransactionID.ToString(),
                    Error = new Error
                    {
                        ErrorCode = 1,
                        ErrorMessage = "Invalid Signature"
                    }
                };
            }
            return new CreateAndConfirmTransactionResponse
            {
                Success = true,
                PartnerTransactionID = partnerTransactionID.ToString(),
                Error = null
            };

        }
    }
}
