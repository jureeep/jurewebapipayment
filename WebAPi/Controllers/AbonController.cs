﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Abon;
using WebAPi.Models;


namespace WebAPi.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class AbonController : ControllerBase
    {
        private IAbonService Service;
        public AbonController(IAbonService service)
        {
            Service = service;
        }

        [HttpPost]
        public async Task<IActionResult> ConfirmTransaction([FromBody]ConfirmTransactionRequest request) {
          var response=await Service.ConfirmTransaction(request.CouponCode);
            return Ok(response);
        }

        [HttpPost]      
        public async Task<IActionResult> ValidateCoupon(ValidateCouponRequest request) {
            var response = await Service.ValidateCoupon(request.CouponCode);
            return Ok(response);
        }
        [HttpPost]
        public async Task<IActionResult> CreateCoupon(CreateCouponRequest request) {
            var response = await Service.CreateCoupon(request.Value,request.PointOfSaleId,request.IsoCurrencySymbol);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> CancelCoupon(CancelCouponRequest request)
        {
            var response = await Service.CancelCoupon(request.PointOfSaleId,request.SerialNumber);
            return Ok(response);
        }


    }
}