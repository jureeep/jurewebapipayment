﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AircashPayoutService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPi.Models;

namespace WebAPi.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class AircashPayoutController : ControllerBase
    {
        private IAircashPayoutService PayoutService;
        public AircashPayoutController(IAircashPayoutService payoutService)
        {
            PayoutService = payoutService;
        }

        [HttpPost]
        public async Task<IActionResult> CheckUser(CheckUserRequest request)
        {
            var response=await PayoutService.CheckUser(request.PhoneNumber);
            return Ok(response);
        }
        [HttpPost]
        public async Task<IActionResult> CreatePayout(CreatePayoutRequest request)
        {
            var response = await PayoutService.CreatePayout(request.PhoneNumber,request.Amount);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> CheckStatus(CheckStatusRequest request)
        {
            var response = await PayoutService.CheckStatus(request.PartnerTransactionID, request.AircashTransactionID);
            return Ok(response);
        }

    }
}