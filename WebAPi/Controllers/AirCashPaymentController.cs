﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.AircashPayment;
using WebAPi.Models;

namespace WebAPi.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class AircashPaymentController : ControllerBase
    {
        private IAircashPaymentService PaymentService;
        public AircashPaymentController(IAircashPaymentService aircashPaymentService)
        {
            PaymentService = aircashPaymentService;
        }
        [HttpPost]
        public IActionResult CheckPlayer(CheckPlayerRequest request)
        {
            return Ok(PaymentService.CheckPlayer(request.Parameters, request.Signature));
        }

        [HttpPost]
        public IActionResult CreateAndConfirmPayment(CreateAndConfirmTransactionRequest request)
        {
            return Ok(PaymentService.CreateAndConfirmTransaction(request.Amount,request.Parameters,request.TransactionID,request.Signature));
        }
    }
}