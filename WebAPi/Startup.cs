﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AircashPayoutService;
using CrossCutting;
using DataAccess;
using HttpRequestService;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Services.Abon;
using Services.AircashPayment;
using Services.DataGenerator;
using SignatureService;
using TemplateService;

namespace WebAPi
{
    public class Startup
    {
    
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
          
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddTransient<IAbonService, AbonService>();
            services.AddTransient<ISignatureService,SignatureService.SignatureService>();
            services.AddTransient<IHttpRequestService, HttpRequestService.HttpRequestService>();
            services.AddTransient<ITemplateService,TemplateService.TemplateService>();
            services.AddTransient<IAircashPayoutService,AircashPayoutService.AircashPayoutService>();
            services.AddTransient<IAircashPaymentService, AircashPaymentService>();
            services.AddTransient<IDataGeneratorService, DataGeneratorService>();
            services.Configure<AbonConfiguration>(Configuration.GetSection("AbonConfiguration"));
            services.Configure<AircashConfiguration>(Configuration.GetSection("AircashConfiguration"));
            services.AddDbContext<VirtualDbContext>(options => options.UseInMemoryDatabase(databaseName: "VirtualProvider"));
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,IDataGeneratorService dataGeneratorService)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }    
            app.UseHttpsRedirection();
            app.UseMvc();
            dataGeneratorService.AddData();
            
        }
    }
}
