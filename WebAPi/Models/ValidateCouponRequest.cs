﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPi.Models
{
    public class ValidateCouponRequest
    {
        public string CouponCode { get; set; }
    }
}
