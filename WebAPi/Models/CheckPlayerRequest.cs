﻿using Services.AircashPayment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPi.Models
{
    public class CheckPlayerRequest
    {
        public List<ParameterPayment> Parameters { get; set; }
        public string Signature { get; set; }
    }
}
