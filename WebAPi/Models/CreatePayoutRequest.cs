﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPi.Models
{
    public class CreatePayoutRequest
    {
        public decimal Amount { get; set; }
        public string PhoneNumber { get; set; }
    }
}
