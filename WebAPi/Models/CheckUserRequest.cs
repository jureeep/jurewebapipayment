﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPi.Models
{
    public class CheckUserRequest
    {
        public string PhoneNumber { get; set; }
    }
}
