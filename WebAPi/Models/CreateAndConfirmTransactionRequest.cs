﻿using Services.AircashPayment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPi.Models
{
    public class CreateAndConfirmTransactionRequest
    {
        public string TransactionID { get; set; }
        public decimal Amount { get; set; }
        public List<ParameterPayment> Parameters { get; set; }
        public string Signature { get; set; }
    }
}
