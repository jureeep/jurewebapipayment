﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPi.Models
{
    public class CheckStatusRequest
    {
        public string PartnerTransactionID { get; set; }
        public string AircashTransactionID { get; set; }
    }
}
