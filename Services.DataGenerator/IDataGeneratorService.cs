﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.DataGenerator
{
   public interface IDataGeneratorService
    {
        void AddData();
    }
}
