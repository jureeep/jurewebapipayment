﻿using DataAccess;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.DataGenerator
{
    public class DataGeneratorService : IDataGeneratorService
    {
        private VirtualDbContext Context;
        public DataGeneratorService(VirtualDbContext virtualDbContext)
        {
            Context = virtualDbContext;
        }

        public void AddData()
        {
            Context.Database.EnsureCreated();
            var users = new User[] {
                new User{ ID=1,FirstName="Jure",LastName="Pinjuh",Username="jurepinjuh",Email="jure@gmail.com",Status=UserStatus.Verified },
                new User{ ID=2,FirstName="Pero",LastName="Perić",Username="peroperic",Email="pero@gmail.com",Status=UserStatus.Verified },
                new User{ ID=3,FirstName="Mirko",LastName="Mirkic",Username="mirkomirkic",Email="mirko@gmail.com",Status=UserStatus.Verified },
                new User{ ID=4,FirstName="Marko",LastName="Markic",Username="markomarkic",Email="marko@gmail.com",Status=UserStatus.Verified },
                new User{ ID=5,FirstName="Ivan",LastName="Ivic",Username="ivoivic",Email="ivo@gmail.com",Status=UserStatus.Verified },
                new User{ ID=6,FirstName="Tina",LastName="Tinic",Username="tinatinic",Email="tina@gmail.com",Status=UserStatus.Verified }            
            };
            Array.ForEach(users,user=>Context.Add(user));
            Context.SaveChanges();      
        }
    }
}
