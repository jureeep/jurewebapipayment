﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SignatureService.Enum
{
    public enum SignatureEnum
    {
        Abon = 1,
        AirCashPayout = 2,
        AirCashPayment = 3
    }
}
