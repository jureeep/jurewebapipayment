﻿using CrossCutting;
using Microsoft.Extensions.Options;
using SignatureService.Enum;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SignatureService
{
   public class SignatureService : ISignatureService
    {
        public AbonConfiguration Config;
        public SignatureService(IOptionsMonitor<AbonConfiguration> abonConfiguration)
        {
            Config = abonConfiguration.CurrentValue;
        }
        public string GenerateSignature(string toSign,SignatureEnum signatureEnum)
        {
            var certificate = new X509Certificate2(Config.Path, Config.Pass);

            string xml = Config.PublicXMLKey;
            if (signatureEnum==SignatureEnum.Abon)
            {
                toSign = $"{toSign}{xml}";
            }
                     
            string signature;
            using (RSA rsa = certificate.GetRSAPrivateKey())
            {
                var dataToSign = Encoding.UTF8.GetBytes(toSign);
                byte[] signedData;
                if (signatureEnum == SignatureEnum.Abon)
                {
                    signedData = rsa.SignData(dataToSign, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);
                }
                else {
                    signedData = rsa.SignData(dataToSign, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
                }

                signature = Convert.ToBase64String(signedData);
            }
            return signature;
        }

        public bool VerifySignature(string certificatePath, string signature, string dataToVerify)
        {
            var certificate = new X509Certificate2(certificatePath);
            var dataToVerifyBytes = Encoding.UTF8.GetBytes(dataToVerify);
            var signatureBytes = Convert.FromBase64String(signature);
            using (var rsaAlg = certificate.GetRSAPublicKey())
            { return rsaAlg.VerifyData(dataToVerifyBytes, signatureBytes, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1); }

        }
    }
}
