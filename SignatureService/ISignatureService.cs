﻿using SignatureService.Enum;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SignatureService
{
    public interface ISignatureService
    {
        string GenerateSignature(string toSign,SignatureEnum signatureEnum);
        bool VerifySignature(string certificatePath, string signature, string dataToVerify);
    }
}
