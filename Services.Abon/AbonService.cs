﻿using CrossCutting;
using HttpRequestService;
using Microsoft.Extensions.Options;
using Services.Abon.Models;
using SignatureService;
using SignatureService.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using TemplateService;

namespace Services.Abon
{
   public class AbonService :IAbonService
    {   
        private AbonConfiguration AbonConfig;
        private readonly string ContentType = "pdf";
        private readonly int ContentWidth = 20;
        private ISignatureService SignatureService;
        private IHttpRequestService HttpService;
        private ITemplateService TemplateService;
        private readonly SignatureEnum SignatureEnum = SignatureEnum.Abon;
        public AbonService(IOptionsMonitor<AbonConfiguration> abonConfiguration,ISignatureService signatureService,IHttpRequestService httpService,ITemplateService templateService)
        {
            AbonConfig = abonConfiguration.CurrentValue;
            SignatureService = signatureService;
            HttpService = httpService;
            TemplateService = templateService;
        }

        public async Task<CancelCouponResponse> CancelCoupon(string pointOfSaleId,string serialNumber)
        {
     
            var toSign=AbonConfig.PartnerId.ToString().ToUpper() + serialNumber.ToUpper() + pointOfSaleId.ToUpper();
            var signed = SignatureService.GenerateSignature(toSign,SignatureEnum);
            string template=AbonConfig.CouponCancelSoap;
            var obj = new
            {
                partnerId = AbonConfig.PartnerId,
                signed=signed,
                pointOfSaleId=pointOfSaleId,
                serialNumber=serialNumber
            };
            string soapXml = await TemplateService.RenderTemplateAsync(template,obj);
            var soapResponse= await HttpService.SendRequestAbon(soapXml,HttpMethod.Post,AbonConfig.AbonCreationUri,AbonConfig.CouponCancelAction);
            return Serializer.ParseXML<CancelCouponResponse>(soapResponse);
        }

        public async Task<ConfirmTransactionResponse> ConfirmTransaction(string couponCode)
        {
            var providerTransactionId = Guid.NewGuid();
            var providerID = AbonConfig.ProviderId;
            var userID = Guid.NewGuid();
            string toSign = couponCode.ToString().ToUpper()+providerID.ToString().ToUpper() + providerTransactionId.ToString().ToUpper();
            string signed =  SignatureService.GenerateSignature(toSign,SignatureEnum);
            string template = AbonConfig.ConfirmTransactionSoap;
            var obj = new {
                providerTransactionId=providerTransactionId,
                providerID=providerID,
                userID=userID,
                signed=signed,
                couponCode=couponCode
            };
            string soapXml=await TemplateService.RenderTemplateAsync(template,obj);
            var xmlResponse = await HttpService.SendRequestAbon(soapXml, HttpMethod.Post, AbonConfig.AbonUri, AbonConfig.ConfirmSoapAction);
            return Serializer.ParseXML<ConfirmTransactionResponse>(xmlResponse);
        }

        public async Task<CreateCouponResponse> CreateCoupon(decimal Value, string PointOfSaleId, string IsoCurrencySymbol)
        {
            var partnerId = AbonConfig.PartnerId;
            var partnerTransactionId = Guid.NewGuid();
      
            string toSign=  partnerId.ToString().ToUpper() +
                            Value.ToString().ToUpper() + PointOfSaleId.ToUpper() +
                            IsoCurrencySymbol.ToUpper() +
                            partnerTransactionId.ToString().ToUpper() +
                            ContentType.ToString().ToUpper() + ContentWidth.ToString().ToUpper();
            string signed = SignatureService.GenerateSignature(toSign,SignatureEnum);
            string template = AbonConfig.CreateCouponSoap;
            var obj =new {
                partnerId=partnerId,
                partnerTransactionId=partnerTransactionId,
                Value=Value,
                PointOfSaleId=PointOfSaleId,
                IsoCurrencySymbol=IsoCurrencySymbol,
                signed=signed,
                contentType=ContentType,
                contentWidth=ContentWidth
            };
            string soapXml = await TemplateService.RenderTemplateAsync(template,obj);
            var soapResponse= await HttpService.SendRequestAbon(soapXml,HttpMethod.Post,AbonConfig.AbonCreationUri,AbonConfig.CreateSoapAction);
            return Serializer.ParseXML<CreateCouponResponse>(soapResponse);
        }

        public async Task<ValidateCouponResponse> ValidateCoupon(string couponCode)
        {
            var providerID = AbonConfig.ProviderId;
            string toSign = couponCode.ToString().ToUpper()+providerID.ToString().ToUpper();
            string signed =SignatureService.GenerateSignature(toSign,SignatureEnum);
            string template = AbonConfig.CouponValidateSoap;
            var obj = new {
                providerID=providerID,
                couponCode=couponCode,
                signed=signed
            };
            string soapXml = await TemplateService.RenderTemplateAsync(template,obj);
            var soapResponse= await HttpService.SendRequestAbon(soapXml, HttpMethod.Post, AbonConfig.AbonUri, AbonConfig.ValidateSoapAction);
            return Serializer.ParseXML<ValidateCouponResponse>(soapResponse);
        }
    }
}
