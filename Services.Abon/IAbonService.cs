﻿using Services.Abon.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abon
{
  public interface IAbonService
    {
         Task<ConfirmTransactionResponse> ConfirmTransaction(string couponCode);
        Task<ValidateCouponResponse> ValidateCoupon(string couponCode);
        Task<CreateCouponResponse> CreateCoupon(decimal Value,string PointOfSaleId,string IsoCurrencySymbol);
        Task<CancelCouponResponse> CancelCoupon(string PointOfSaleId,string serialNumber);
    }
}
