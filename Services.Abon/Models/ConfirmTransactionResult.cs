﻿using System.Xml.Serialization;

namespace Services.Abon.Models
{
  
    public class ConfirmTransactionResult
    {
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public bool Success { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public decimal CouponValue { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string ISOCountryCode { get; set; }
        [XmlElement( Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string ISOCurrency { get; set; }
        [XmlElement( Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string ProviderTransactionId { get; set; }
        [XmlElement( Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public int Code { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string Message { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string SalePartnerId { get; set; }
    }
}