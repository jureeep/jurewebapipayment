﻿using System.Xml.Serialization;

namespace Services.Abon.Models
{
    public class ValidateCouponResult
    {
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public decimal CouponValue { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public bool IsValid { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string ISOCountryCode { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string ISOCurrency { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string ProviderTransactionId { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public decimal OriginalCouponValue { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string OriginalISOCurrency { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public int Code { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string Message { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string SalePartnerId { get; set; }
    }
}