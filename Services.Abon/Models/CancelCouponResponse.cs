﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Services.Abon.Models
{
    [XmlRoot("CancelCouponResponse", Namespace = "http://tempuri.org/")]
    public class CancelCouponResponse
    {
        [XmlElement(ElementName = "CancelCouponResult")]
        public CancelCouponResult CancelCouponResult { get; set; }
     
    }
}
