﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Services.Abon.Models
{
    [XmlRoot("CreateCouponResponse", Namespace = "http://tempuri.org/")]
    public class CreateCouponResponse
    {
        [XmlElement(ElementName = "CreateCouponResult")]
        public CreateCouponResult CreateCouponResult { get; set; }
     
    }
}
