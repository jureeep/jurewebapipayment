﻿using System.Xml.Serialization;

namespace Services.Abon.Models
{
    public class CreateCouponResult
    {
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string SerialNumber { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public decimal Value { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string ISOCurrencySymbol { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public byte[] Content { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string PartnerTransactionId { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public bool Success { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string CouponCode { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public int Code { get; set; }
        [XmlElement(Namespace = "http://schemas.datacontract.org/2004/07/EbonService.DataContracts")]
        public string Message { get; set; }
    }
}