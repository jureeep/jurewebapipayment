﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Services.Abon.Models
{
    [XmlRoot("ConfirmTransactionResponse",Namespace = "http://tempuri.org/")]
    public class ConfirmTransactionResponse
    {
        [XmlElement(ElementName ="ConfirmTransactionResult")]
        public ConfirmTransactionResult ConfirmTransactionResult { get; set; }
     
    }
}
