﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Services.Abon.Models
{
    [XmlRoot("ValidateCouponResponse", Namespace = "http://tempuri.org/")]
    public class ValidateCouponResponse
    {
        [XmlElement(ElementName = "ValidateCouponResult")]
        public ValidateCouponResult ValidateCouponResult { get; set; }
       
    }
}
