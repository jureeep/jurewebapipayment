﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HttpRequestService
{
   public interface IHttpRequestService
    {
        Task<string> SendRequestAbon(string soapEnvelope,HttpMethod method,string uri,string soapAction);
        Task<string> SendRequestAircash(object toSend, HttpMethod method, string uri);
    }
}
