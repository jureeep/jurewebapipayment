﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HttpRequestService
{
    public class HttpRequestService : IHttpRequestService
    {
       

        public async Task<string> SendRequestAbon(string soapEnvelope, HttpMethod method, string uri, string soapAction)
        {
            var httpClient = new HttpClient();
            string responseContent;
            using (var request = new HttpRequestMessage(method, uri))
            {
                request.Content = new StringContent(soapEnvelope, Encoding.UTF8, "text/xml");
                request.Headers.Add("SOAPAction", soapAction);
                using (var response = await httpClient.SendAsync(request))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception(response.StatusCode.ToString());
                    }

                    responseContent = await response.Content.ReadAsStringAsync();
                }
            };
            return responseContent;
        }

        public async Task<string> SendRequestAircash(object toSend, HttpMethod method, string uri)
        {
            var handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback =
                (message, certificate, chain, sslPolicyErrors) => true;
            var httpClient = new HttpClient(handler);
            string responseContent;
            using (var request = new HttpRequestMessage(method, uri))
            {
                string json = JsonConvert.SerializeObject(toSend);
                request.Content = new StringContent(json, Encoding.UTF8, "application/json");
                using (var response = await httpClient.SendAsync(request))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception(response.StatusCode.ToString());
                    }

                    responseContent = await response.Content.ReadAsStringAsync();
                }
            };
            return responseContent;
        }
    }
}
