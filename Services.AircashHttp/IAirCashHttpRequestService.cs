﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Services.AircashHttp
{
   public interface IAirCashHttpRequestService
    {
        Task<string> SendRequestAsync(object toSend,HttpMethod method,string uri);
    }
}
