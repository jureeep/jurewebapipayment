﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AircashPayoutService.Models
{
    public class AircashCreatePayoutRequest
    {
        public string PartnerID { get; set; }
        public string PhoneNumber { get; set; }
        public string Signature { get; set; }
        public decimal Amount { get; set; }
        public string PartnerTransactionID { get; set; }
    }
}
