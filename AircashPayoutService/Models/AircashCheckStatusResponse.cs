﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AircashPayoutService.Models
{
    public class AircashCheckStatusResponse
    {
        public bool Status { get; set; }
        public string AircashTransactionID { get; set; }
    }
}
