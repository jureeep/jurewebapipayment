﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AircashPayoutService.Models
{
   public  class AircashCreatePayoutResponse
    {
        public string AircashTransactionID { get; set; }
    }
}
