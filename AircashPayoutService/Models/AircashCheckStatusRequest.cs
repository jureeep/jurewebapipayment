﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AircashPayoutService.Models
{
    public class AircashCheckStatusRequest
    {
        public string PartnerID { get; set; }
        public string PartnerTransactionID { get; set; }
        public string AircashTransactionID { get; set; }
        public string Signature { get; set; }
    }
}
