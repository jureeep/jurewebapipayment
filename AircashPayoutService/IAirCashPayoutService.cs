﻿using AircashPayoutService.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AircashPayoutService
{
   public interface IAircashPayoutService
    {
        Task<AircashCheckUserResponse> CheckUser(string phoneNumber);
        Task<AircashCreatePayoutResponse> CreatePayout(string phoneNumber, decimal amount);
        Task<AircashCheckStatusResponse> CheckStatus(string partnerTransactionID,string aircashTransactionID);
    }
}
