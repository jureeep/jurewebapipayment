﻿using CrossCutting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AircashPayoutService.Models;
using SignatureService;
using System.Net.Http;
using SignatureService.Enum;
using HttpRequestService;
using Newtonsoft.Json;

namespace AircashPayoutService
{
    public class AircashPayoutService : IAircashPayoutService
    {
        private AircashConfiguration AirCashConfig;
        private ISignatureService SignatureService;
        private IHttpRequestService HttpService;
        private readonly SignatureEnum SignatureEnum = SignatureEnum.AirCashPayout; 
        public AircashPayoutService(IOptionsMonitor<AircashConfiguration> aircashConfiguration,ISignatureService signatureService,IHttpRequestService httpService)
        {
            AirCashConfig = aircashConfiguration.CurrentValue;
            SignatureService = signatureService;
            HttpService = httpService;
        }

        public async Task<AircashCheckUserResponse> CheckUser(string phoneNumber)
        {
            string PartnerID = AirCashConfig.PartnerID;
            string toSign = $"PartnerID={PartnerID}&PersonalID=&PhoneNumber={phoneNumber}";
            string signed = SignatureService.GenerateSignature(toSign,SignatureEnum);
            AircashCheckUserRequest requestObject = new AircashCheckUserRequest {
                PartnerID = PartnerID,
                PhoneNumber = phoneNumber,
                Signature = signed,
            };
            string uri = AirCashConfig.AircashUriRoot + AirCashConfig.CheckUserAction;
            var jsonResponse= await HttpService.SendRequestAircash(requestObject,HttpMethod.Post,uri);
            return JsonConvert.DeserializeObject<AircashCheckUserResponse>(jsonResponse);

        }

        public async Task<AircashCreatePayoutResponse> CreatePayout(string phoneNumber, decimal amount)
        {
            string PartnerID = AirCashConfig.PartnerID;
            var PartnerTransactionID = Guid.NewGuid();
            string toSign = $"Amount={amount}&PartnerID={PartnerID}&PartnerTransactionID={PartnerTransactionID.ToString()}&PersonalID=&PhoneNumber={phoneNumber}";
            string signed = SignatureService.GenerateSignature(toSign, SignatureEnum);
            AircashCreatePayoutRequest requestObject = new AircashCreatePayoutRequest {
                PartnerID=PartnerID,
                PhoneNumber=phoneNumber,
                Signature=signed,
                Amount=amount,
                PartnerTransactionID=PartnerTransactionID.ToString()
            };
            string uri = AirCashConfig.AircashUriRoot + AirCashConfig.CreatePayoutAction;
            var jsonResponse= await HttpService.SendRequestAircash(requestObject, HttpMethod.Post, uri);
            return JsonConvert.DeserializeObject<AircashCreatePayoutResponse>(jsonResponse);
        }

        public async Task<AircashCheckStatusResponse> CheckStatus(string partnerTransactionID, string aircashTransactionID)
        {
            string PartnerID = AirCashConfig.PartnerID;
            
            string toSign = $"AircashTransactionID={aircashTransactionID}&PartnerID={PartnerID}&PartnerTransactionID={partnerTransactionID}";
            string signed = SignatureService.GenerateSignature(toSign, SignatureEnum);
            AircashCheckStatusRequest requestObject = new AircashCheckStatusRequest {
                PartnerID=PartnerID,
                PartnerTransactionID=partnerTransactionID,
                Signature=signed,
                AircashTransactionID=aircashTransactionID
            };
            string uri = AirCashConfig.AircashUriRoot + AirCashConfig.CheckStatusPayoutAction;
            var jsonResponse= await HttpService.SendRequestAircash(requestObject, HttpMethod.Post, uri);
            return JsonConvert.DeserializeObject<AircashCheckStatusResponse>(jsonResponse);
        }
    }
}
